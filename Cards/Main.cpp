//Bennet Thompson
#include <iostream>
#include <conio.h>
#include <string>

using namespace std;

enum Rank
{
	TWO = 2, THREE, FOUR, FIVE, SIX, SEVEN, EIGHT, NINE, TEN, JACK, QUEEN, KING, ACE
};
enum Suit
{
	SPADES, DIAMOND, CLUB, HEARTS
};

struct Card
{
	Rank rank;
	Suit suit;
};


void PrintCard(Card printCard)
{
	string printCardRank = "";
	string printCardSuit = "";

	switch (printCard.rank)
	{
	case 2: printCardRank = "Two";
		break;
	case 3: printCardRank = "Three";
		break;
	case 4: printCardRank = "Four";
		break;
	case 5: printCardRank = "Five";
		break;
	case 6: printCardRank = "Six";
		break;
	case 7: printCardRank = "Seven";
		break;
	case 8: printCardRank = "Eight";
		break;
	case 9: printCardRank = "Nine";
		break;
	case 10: printCardRank = "Ten";
		break;
	case 11: printCardRank = "Jack";
		break;
	case 12: printCardRank = "Queen";
		break;
	case 13: printCardRank = "King";
		break;
	case 14: printCardRank = "Ace";
		break;
	}

	switch (printCard.suit)
	{
	case 0: printCardSuit = "Spades";
		break;
	case 1: printCardSuit = "Diamonds";
		break;
	case 2: printCardSuit = "Clubs";
		break;
	case 3: printCardSuit = "Hearts";
		break;
	}



	cout << "Card is a " + printCardRank + " of " + printCardSuit;
}

Card HighCard(Card c1, Card c2)
{
	Card highcard;
	if (c1.rank > c2.rank)
	{
		highcard.rank = c1.rank;
		highcard.suit = c1.suit;
		return c1;
	}
	else
	{
		highcard.rank = c2.rank;
		highcard.suit = c2.suit;
		return c2;
	}

}

int main()
{

	Card c1;
	c1.rank = SIX;
	c1.suit = HEARTS;

	Card c2;
	c2.rank = QUEEN;
	c2.suit = SPADES;

	Card printCard;
	printCard = HighCard(c1, c2);
	PrintCard(printCard);

	_getch();
	return 0;
}
